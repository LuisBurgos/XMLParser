    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

import java.util.*;
import java.io.*;

/**
 *
 * @author luisburgos
 */
public class SAXParser {

    public SAXParser() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {

            InputStream xmlInput = new FileInputStream("simple.xml");
            javax.xml.parsers.SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new SaxHandler();
            saxParser.parse(xmlInput, handler);

        } catch (Throwable err) {
            err.printStackTrace();
        }
    }

}

class SaxHandler extends DefaultHandler {

    public void startDocument() throws SAXException {
        System.out.println("start document   : ");
    }

    public void endDocument() throws SAXException {
        System.out.println("end document     : ");
    }

    public void startElement(String uri, String localName,
            String qName, Attributes attributes)
            throws SAXException {

        int length = attributes.getLength();

        for (int i = 0; i < length; i++) {
            String name = attributes.getQName(i);
            System.out.println("Name:" + name);
            String value = attributes.getValue(i);
            System.out.println("Value:" + value);
            String nsUri = attributes.getURI(i);
            System.out.println("NS Uri:" + nsUri);
            String lName = attributes.getLocalName(i);
            System.out.println("Local Name:" + lName);
        }

        System.out.println("start element    : " + qName);
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        System.out.println("end element      : " + qName);
    }

    public void characters(char ch[], int start, int length)
            throws SAXException {
        System.out.println("start characters : "
                + new String(ch, start, length));
    }

}
