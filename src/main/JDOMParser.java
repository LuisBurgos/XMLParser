/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 *
 * @author luisburgos
 */
public class JDOMParser {

    SAXBuilder builder = new SAXBuilder();
    File xmlFile = new File("simple.xml");

    public JDOMParser()
    {
        try {

            Document document = (Document) builder.build(xmlFile);
            Element rootNode = document.getRootElement();

            String cadena = "Elemento: " + rootNode.getName() + " Valor: " 
                        + rootNode.getTextTrim() + " Padre: " + rootNode.getParent().toString();
            
            if(!rootNode.getChildren().isEmpty()){
                imprimirElementos(rootNode.getChildren());
            }

            System.out.println(cadena);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }



public void imprimirElementos(List list){
        
        for (Iterator it = list.iterator(); it.hasNext();) {
                Element element = (Element) it.next();
                
                System.out.println("");
                String cadena = "Elemento: " + element.getName() + " Valor: " 
                        + element.getTextTrim() + " Padre: " + element.getParent().toString();
                
                
                if(!element.getAttributes().isEmpty()){
                    //System.out.println(element.getAttributes().toString());
                    for (Iterator ite = element.getAttributes().iterator(); ite.hasNext();) {                        
                        Attribute att = (Attribute) ite.next();
                        System.out.println("Att name: " + att.getName()  +
                                            "/ Att value: "+ att.getValue());
                        //System.out.println(att.toString());
                    }
                }
                
                if (!element.getChildren().isEmpty()) {
                    imprimirElementos(element.getChildren());
                }
                

                System.out.println(cadena);
        }
    }

}
